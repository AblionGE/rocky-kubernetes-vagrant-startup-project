# Rocky Kubernetes vagrant startup project

This project aims to get a quick and non-secure way to create VM within Virtualbox with Vagrant to start playing with Kubernetes.

## HOW-TO

We wille create 3 virtual machines:

- A single Control plane: `cp1`
- Two worker nodes: `w01` and `w02`

1. [Install Virtualbox on your machine](https://www.virtualbox.org/wiki/Downloads)
2. [Install Vagrant on your machine](https://developer.hashicorp.com/vagrant/tutorials/getting-started/getting-started-install)
3. If you're working on WSL2 (Windows + Ubuntu), you have to install Virtualbox on Windows and Vagrant on Linux. See [here](https://thedatabaseme.de/2022/02/20/vagrant-up-running-vagrant-under-wsl2/) for more details.
4. Update `Vagrantfile` replacing specific path on line 6.
5. ```vagrant up```
6. Once the VMs are up, go up in your terminal logs to get the following lines from your cp1 machine:
```bash
kubeadm join 192.168.56.2:6443 --token 7q9vx4.917mr2clcw996h2l \
  --discovery-token-ca-cert-hash sha256:f12c32154169bb4da0e993f2880a938ea3cfe2fceb5a40fec6d06911bf962e08
```
7. Connect to `w01` and `w02` using `vagrant ssh w0X` and run the command from previous step as `root` user.
8. Connect to cp1 `vagrant ssh cp1`, become root (`sudo su`) and run `kubectl get nodes`.
You should see your two worker nodes.
