# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky8"
  config.vm.synced_folder '/mnt/c/Users/marc1/Vagrant/', '/vagrant', disabled: true

  config.vm.define "cp1" do |cp1|
    cp1.vm.network "private_network", ip: "192.168.56.2"
    cp1.vm.provider "virtualbox" do |vb|
      vb.memory = 4096
      vb.cpus = 4
    end
    cp1.vm.provision "shell", inline: <<-SHELL
      hostname cp1
      echo "cp1" > /etc/hostname
      kubeadm init --pod-network-cidr 192.168.57.0/24 --apiserver-advertise-address=192.168.56.2

      mkdir -p $HOME/.kube
      cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
      chown $(id -u):$(id -g) $HOME/.kube/config

      curl https://raw.githubusercontent.com/projectcalico/calico/v3.24.5/manifests/calico.yaml -O
      sed -i 's/.*CALICO_IPV4POOL_CIDR/            - name: CALICO_IPV4POOL_CIDR/g' calico.yaml
      sed -i 's/.*192.168.0.0.*/              value: "192.168.57.0\/24"/g' calico.yaml
      kubectl apply -f calico.yaml
    SHELL
  end
  config.vm.define "w01" do |w01|
    w01.vm.network "private_network", ip: "192.168.56.5"
    w01.vm.provision "shell", inline: <<-SHELL
      hostname w01
      echo "w01" > /etc/hostname
    SHELL
  end
  config.vm.define "w02" do |w02|
    w02.vm.network "private_network", ip: "192.168.56.6"
    w02.vm.provision "shell", inline: <<-SHELL
      hostname w02
      echo "w02" > /etc/hostname
    SHELL
  end

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "4096"
    vb.cpus = "3"
  end

  config.vm.provision "shell", inline: <<-SHELL
    echo "192.168.56.2 cp1" >> /etc/hosts
    echo "192.168.56.5 w01" >> /etc/hosts
    echo "192.168.56.6 w02" >> /etc/hosts
    swapoff -a
    sed -i '/ swap / s/^/#/' /etc/fstab
    dnf upgrade -y
    dnf install -y htop vim epel-release net-tools bind-utils iproute-tc
    modprobe br_netfilter
    sysctl -w net.ipv4.ip_forward=1
    dnf install -y containerd.io
    cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
    setenforce 0
    sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

    dnf install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

    mkdir -p /etc/containerd && containerd config default > /etc/containerd/config.toml && systemctl restart containerd
    systemctl enable --now kubelet
    systemctl disable --now firewalld

    systemctl daemon-reload
    systemctl restart kubelet

  SHELL
end
